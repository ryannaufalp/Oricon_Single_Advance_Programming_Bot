package advprog.bot.feature.oriconsingle.handler;

import advprog.bot.feature.echo.EchoChatHandler;
import advprog.bot.feature.oriconsingle.util.commands.ChartCommandControl;
import advprog.bot.line.AbstractLineChatHandlerDecorator;
import advprog.bot.line.LineChatHandler;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.AudioMessageContent;
import com.linecorp.bot.model.event.message.ImageMessageContent;
import com.linecorp.bot.model.event.message.LocationMessageContent;
import com.linecorp.bot.model.event.message.StickerMessageContent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import org.springframework.stereotype.Service;


@Service
public class OriconSingleChatHandler extends AbstractLineChatHandlerDecorator {

    private static final Logger LOGGER = Logger
            .getLogger(EchoChatHandler.class.getName());
    private final String invalidFormatMessage = "Sorry-masen! your input is not valid!\n"
            + "Please enter these format: \n\n"
            + "/oricon jpsingles [daily/weekly] [yyyy-mm-dd]\n"
            + "for daily and weekly\n\n"
            + "or\n\n"
            + "/oricon jpsingles [yyyy-mm]/[yyyy]\n"
            + "for monthly and yearly";

    public OriconSingleChatHandler(LineChatHandler decoratedHandler) {
        this.decoratedLineChatHandler = decoratedHandler;
        LOGGER.info("Oricon Single chat handler added!");
    }

    @Override
    protected boolean canHandleTextMessage(MessageEvent<TextMessageContent> event) {
        String[] message = event.getMessage().getText().split(" ");
        return message[0].equalsIgnoreCase("/oricon")
                && message[1].equalsIgnoreCase("jpsingles");
    }

    @Override
    protected List<Message> handleTextMessage(MessageEvent<TextMessageContent> event) {
        ChartCommandControl control = new ChartCommandControl();
        try {
            String[] message = event.getMessage().getText().split(" ");
            if (message.length == 4) {
                return Collections.singletonList(control.execute(message[2], message[3]));
            } else if (message.length == 3) {
                String[] date = message[2].split("-");
                if (date.length == 2) {
                    return Collections.singletonList(control.execute("monthly", message[2]));
                } else if (date.length == 1) {
                    return Collections.singletonList(control.execute("yearly", message[2]));
                } else {
                    throw new IllegalArgumentException();
                }
            } else {
                throw new IllegalArgumentException();
            }
        } catch (IOException e) {
            return new LinkedList<>();
        } catch (IllegalArgumentException e) {
            return Collections.singletonList(
                    new TextMessage(invalidFormatMessage));
        }
    }

    @Override
    protected boolean canHandleImageMessage(MessageEvent<ImageMessageContent> event) {
        return false;
    }

    @Override
    protected boolean canHandleAudioMessage(MessageEvent<AudioMessageContent> event) {
        return false;
    }

    @Override
    protected boolean canHandleStickerMessage(MessageEvent<StickerMessageContent> event) {
        return false;
    }

    @Override
    protected boolean canHandleLocationMessage(MessageEvent<LocationMessageContent> event) {
        return false;
    }

}
