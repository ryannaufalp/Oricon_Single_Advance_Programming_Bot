package advprog.bot.feature.oriconsingle.handler;

import advprog.bot.BotController;
import advprog.bot.line.LineChatHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OriconSingleChatHandlerConfig {

    @Bean
    OriconSingleChatHandler oriconSingleChatHandler(BotController controller) {
        LineChatHandler chatHandler = controller.getLineChatHandler();
        OriconSingleChatHandler handler =
                new OriconSingleChatHandler(chatHandler);
        controller.replaceLineChatHandler(handler);
        return handler;
    }
}
