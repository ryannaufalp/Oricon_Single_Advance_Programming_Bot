package advprog.bot.feature.oriconsingle.handler;

import static org.junit.Assert.assertFalse;

import static org.junit.jupiter.api.Assertions.assertEquals;

import advprog.bot.ChatHandlerTestUtil;
import advprog.bot.line.BaseChatHandler;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = {OriconSingleChatHandler.class})
public class OriconSingleChatHandlerTest {

    OriconSingleChatHandler handler =
            new OriconSingleChatHandler(new BaseChatHandler());

    private String sampleDailyOutput = "Ryokai desu~ This is your top 10 songs as you "
            + "requested~\n" + "\n(1) 進化理論 - BOYS AND MEN - 2018-05-09 - Not Given\n"
            + "(2) シンクロニシティ - 乃木坂46 - 2018-04-25 - Not Given\n"
            + "(3) Eclipse - 蒼井翔太 - 2018-05-09 - Not Given\n"
            + "(4) この道を/会いに行く/坂道を上って/小さな風景 - 小田和正 - 2018-05-02 - Not Given\n"
            + "(5) THE IDOLM@STER SideM WORLD TRE@SURE 01(永遠(とわ)なる四銃士) - "
            + "天道輝(仲村宗悟),葛之葉雨彦(笠間淳),握野英雄(熊谷健太郎),紅井朱雀(益山武明) - "
            + "2018-05-09 - Not Given\n"
            + "(6) 誓い - 雨宮天 - 2018-05-09 - Not Given\n"
            + "(7) アップデート - miwa - 2018-05-09 - Not Given\n"
            + "(8) 泣けないぜ…共感詐欺/Uraha=Lover/君だけじゃないさ...friends"
            + "(2018アコースティックVer.) - アンジュルム - 2018-05-09 - Not Given\n"
            + "(9) 泡沫夢幻・胡蝶刃 〜GRANBLUE FANTASY〜 - ナルメア(M・A・O) - "
            + "2018-05-02 - Not Given\n"
            + "(10) Crosswalk/リワインド - 鈴木みのり - 2018-05-09 - Not Given";

    private String sampleWeeklyOutput = "Ryokai desu~ This is your top 10 songs as you "
            + "requested~\n" + "\n(1) シンクロニシティ - 乃木坂46 - 2018-04-25 - Not Given\n"
            + "(2) Fandango - THE RAMPAGE from EXILE TRIBE - 2018-04-25 - Not Given\n"
            + "(3) Fiction e.p - sumika - 2018-04-25 - Not Given\n"
            + "(4) Bumblebee - Lead - 2018-04-25 - Not Given\n"
            + "(5) 人間を被る - DIR EN GREY - 2018-04-25 - Not Given\n"
            + "(6) 泣きたいくらい - 大原櫻子 - 2018-04-25 - Not Given\n"
            + "(7) THE IDOLM@STER MILLION THE@TER GENERATION 07 トゥインクルリズム"
            + "(ZETTAI × BREAK!! トゥインクルリズム) - トゥインクルリズム[中谷育(原嶋あかり),"
            + "七尾百合子(伊藤美来),松田亜利沙(村川梨衣)] - 2018-04-25 - Not Given\n"
            + "(8) 春はどこから来るのか? - NGT48 - 2018-04-11 - Not Given\n"
            + "(9) Ask Yourself - KAT-TUN - 2018-04-18 - Not Given\n"
            + "(10) 鍵穴 - the Raid. - 2018-04-25 - Not Given";

    private String sampleMonthlyOutput = "Ryokai desu~ This is your top 10 songs as you "
            + "requested~\n" + "\n(1) シンクロニシティ - 乃木坂46 - 2018-04-25 - 1214510\n"
            + "(2) 早送りカレンダー - HKT48 - 2018-05-02 - 165176\n"
            + "(3) Ask Yourself - KAT-TUN - 2018-04-18 - 149081\n"
            + "(4) 春はどこから来るのか? - NGT48 - 2018-04-11 - 128565\n"
            + "(5) 君のAchoo! - ラストアイドル(シュークリームロケッツ) - 2018-04-18 - 58198\n"
            + "(6) SEXY SEXY/泣いていいよ/Vivid Midnight - Juice=Juice - 2018-04-18 - 54728\n"
            + "(7) ガラスを割れ! - 欅坂46 - 2018-03-07 - 54131\n"
            + "(8) ONE TIMES ONE - コブクロ - 2018-04-11 - 39395\n"
            + "(9) ODD FUTURE - UVERworld - 2018-05-02 - 37347\n"
            + "(10) Shanana ここにおいで - B2takes! - 2018-04-11 - 36455";

    private String sampleYearlyOutput = "Ryokai desu~ This is your top 10 songs as you "
            + "requested~\n" + "\n(1) 願いごとの持ち腐れ - AKB48 - 2017-05-31 - Not Given\n"
            + "(2) #好きなんだ - AKB48 - 2017-08-30 - Not Given\n"
            + "(3) 11月のアンクレット - AKB48 - 2017-11-22 - Not Given\n"
            + "(4) シュートサイン - AKB48 - 2017-03-15 - Not Given\n"
            + "(5) 逃げ水 - 乃木坂46 - 2017-08-09 - Not Given\n"
            + "(6) インフルエンサー - 乃木坂46 - 2017-03-22 - Not Given\n"
            + "(7) いつかできるから今日できる - 乃木坂46 - 2017-10-11 - Not Given\n"
            + "(8) 不協和音 - 欅坂46 - 2017-04-05 - Not Given\n"
            + "(9) 風に吹かれても - 欅坂46 - 2017-10-25 - Not Given\n"
            + "(10) Doors 〜勇気の軌跡〜 - 嵐 - 2017-11-08 - Not Given";

    private String noChartDailyMessage = "Sorry-masen! There's no chart on that date :(";

    private String noChartWeeklyMessage = "Sorry-masen! There's no chart on that date :("
            + " . Make sure the date you assign in on monday!";

    private String noChartMonthlyMessage = "Sorry-masen! There's no chart on that month :(";

    private String noChartYearlyMessage = "Sorry-masen! There's no chart on that year :(";

    private String invalidMessage = "Sorry-masen! your input is not valid!\n\n"
            + "Please enter these format: \n\n"
            + "oricon jpsingles [daily/weekly] [yyyy-mm-dd]\n"
            + "for daily and weekly\n\n"
            + "or\n\n"
            + "oricon jpsingles [yyyy-mm]/[yyyy]\n";

    @Test
    public void testDailyReply() {
        List<Message> expectedMessage = new LinkedList<>();
        expectedMessage.add(new TextMessage(sampleDailyOutput));

        MessageEvent<TextMessageContent> input = ChatHandlerTestUtil
                .fakeMessageEvent("ff", "/oricon jpsingles daily 2018-05-09");

        assertEquals(expectedMessage,
                handler.handleTextMessageEvent(input, expectedMessage));
    }

    @Test
    public void testWeeklyReply() {
        List<Message> expectedMessage = new LinkedList<>();
        expectedMessage.add(new TextMessage(sampleWeeklyOutput));

        MessageEvent<TextMessageContent> input = ChatHandlerTestUtil
                .fakeMessageEvent("aaa", "/oricon jpsingles weekly 2018-05-07");

        assertEquals(expectedMessage,
                handler.handleTextMessageEvent(input, expectedMessage));
    }

    @Test
    public void testMonthlyReply() {
        List<Message> expectedMessage = new LinkedList<>();
        expectedMessage.add(new TextMessage(sampleMonthlyOutput));

        MessageEvent<TextMessageContent> input = ChatHandlerTestUtil
                .fakeMessageEvent("ads", "/oricon jpsingles 2018-04");

        assertEquals(expectedMessage,
                handler.handleTextMessageEvent(input, expectedMessage));
    }

    @Test
    public void testYearlyReply() {
        List<Message> expectedMessage = new LinkedList<>();
        expectedMessage.add(new TextMessage(sampleYearlyOutput));

        MessageEvent<TextMessageContent> input = ChatHandlerTestUtil
                .fakeMessageEvent("fg", "/oricon jpsingles 2017");

        assertEquals(expectedMessage,
                handler.handleTextMessageEvent(input, expectedMessage));
    }

    @Test
    public void testNoChartDailyReply() {
        List<Message> expectedMessage = new LinkedList<>();
        expectedMessage.add(new TextMessage(noChartDailyMessage));

        MessageEvent<TextMessageContent> input = ChatHandlerTestUtil
                .fakeMessageEvent("dj", "/oricon jpsingles daily 2030-05-09");

        assertEquals(expectedMessage,
                handler.handleTextMessageEvent(input, expectedMessage));
    }

    @Test
    public void testNoChartWeeklyReply() {
        List<Message> expectedMessage = new LinkedList<>();
        expectedMessage.add(new TextMessage(noChartWeeklyMessage));

        MessageEvent<TextMessageContent> input = ChatHandlerTestUtil
                .fakeMessageEvent("hj", "/oricon jpsingles weekly 2018-05-06");

        assertEquals(expectedMessage,
                handler.handleTextMessageEvent(input, expectedMessage));
    }

    @Test
    public void testNoChartMonthlyReply() {
        List<Message> expectedMessage = new LinkedList<>();
        expectedMessage.add(new TextMessage(noChartMonthlyMessage));

        MessageEvent<TextMessageContent> input = ChatHandlerTestUtil
                .fakeMessageEvent("fh", "/oricon jpsingles 2030-04");

        assertEquals(expectedMessage,
                handler.handleTextMessageEvent(input, expectedMessage));
    }

    @Test
    public void testNoChartYearlyReply() {
        List<Message> expectedMessage = new LinkedList<>();
        expectedMessage.add(new TextMessage(noChartYearlyMessage));

        MessageEvent<TextMessageContent> input = ChatHandlerTestUtil
                .fakeMessageEvent("df", "/oricon jpsingles 2030");

        assertEquals(expectedMessage,
                handler.handleTextMessageEvent(input, expectedMessage));
    }

    @Test
    public void testInvalidMessage() {
        List<Message> expectedMessage = new LinkedList<>();
        expectedMessage.add(new TextMessage(invalidMessage));

        MessageEvent<TextMessageContent> input = ChatHandlerTestUtil
                .fakeMessageEvent("sda", "sdad");

        assertEquals(expectedMessage,
                handler.handleTextMessageEvent(input, expectedMessage));
    }

    @Test
    public void testIgnoreNonTextMessageEvent() {
        assertFalse(handler.canHandleAudioMessage(null));
        assertFalse(handler.canHandleImageMessage(null));
        assertFalse(handler.canHandleStickerMessage(null));
        assertFalse(handler.canHandleLocationMessage(null));
    }

}
